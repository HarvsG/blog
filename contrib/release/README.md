# pulls release

This is a Go "script" to create a new release blog post from a template.

**NOTE:** Posts could always do with some personal touches. We really are incredibly grateful to each and every person
who helps out the project, big or small.  
This contrib is mostly for automating getting those numbers right in all the spots.

### Usage

From the base directory
```
go run contrib/release/release.go --author jolheiser --milestone 1.12.5
```
